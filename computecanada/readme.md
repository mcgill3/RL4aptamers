# Connect to Compute Canada

A guide on connecting your cpu to the computecanada cluster for remote access

#Using Windows

### Create a computecanada account

Create a [compute canada account](https://www.computecanada.ca/)

### Generating an SSH key for windows

1. Download [MobaXterm](https://mobaxterm.mobatek.net/download.html)
2. Go to tools > MobaXterm SSH Key Generator > select RSA key (2048 bits) > Generate a pubic/private key pair
3. Save both the public and the private keys in folder of choice 

### Set up the SSH Key on Compute Canada

1. Login to your [compute canada account](https://www.computecanada.ca/)
2. Toggle to My Account > Manage SSH Keys 
3. Open the file where the public key was saved 
4. Copy the public key: the key typically starts with "ssh-rsa", "ssh-ed25519" or a similar variant
5. Click on add key
6. Now your desktop has access to compute canada through MobaXterm such that you can login remotely 

### Connecting to a cluster on Compute Canada

1. Determine your [cluster of choice](https://www.computecanada.ca/research-portal/accessing-resources/available-resources)
2. Open MobaXterm
3. On the toolbars, click on Session > click SSH > Under basic SSH settings, enter the remote host ID (narval.computecanada.ca), make sure the boxed is checked for specify username and enter the username 
4. On the same window, click on advanced SSH settings, click on X11 forwarding. 
5. You can either press OK and that will prompt you to the linux terminal where you will enter your password OR you can decide to use the private key you generate so that you are not prompted to enter your password

### You are logged into the cluster, now what?
You will want to run a job on the cluster, so you need to figure out which directory you want to work in. 
Typically, that would be in scratch for test runs and scripts. Refer to the [compute canada resources](https://docs.computecanada.ca/wiki/Storage_and_file_management) for more information.

To maneuver into the scratch folder: 
1) Type ls into the terminal and press enter. You should see two files: projects and scratch 
2) Type cd scratch and press enter 

To move files onto from your local machine onto the cluster, you can either:
1) drag them using the UI
2) use the CLI


